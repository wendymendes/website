---
layout: post
author: Filipe Saraiva
category: pencil-square
title: Inscrições para o curso de extensão "Kotlin para Desenvolvimento Android"
---

O __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) anuncia as inscrições para o curso de extensão "Kotlin para Desenvolvimento Android".

Os pré-requisitos são lógica de programação e ser familiar com conceitos de orientação a objetos.

Serão disponibilizadas 30 vagas e as aulas acontecerão no Labcomp-03 do ICEN/UFPA, nos dias 30 e 31 de Janeiro, das 9:00 às 12:00. Aqueles que comparecerem a todas as aulas terão direito a um certificado de 6 horas.

__Atenção:__

`As inscrições são gratuitas mas aqueles que se inscreverem e faltarem uma ou mais aulas ficarão impossibilitados de se inscrever em outros cursos ofertados pelo CCSL-UFPA no futuro.`

Vamos todos colaborar para que esse projeto tenha sucesso e aconteça por várias edições.

__Serviço:__

* __Atividade__: Curso "Kotlin para Desenvolvimento Android"
* __Data__: de 30 à 31 de janeiro de 2019
* __Horário__: das 9:00 às 12:00.
* __Local__: Labcomp-03 do ICEN-UFPA (Rua Augusto Corrêa n. 01 - Guamá)

__Formulário de Inscrições:__

<div class="embed-responsive embed-responsive-16by9">
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfFavzTvwWjDzT-Ym0_XlK9DQSTWXIWE15St-QMTJGO6th3OQ/viewform?embedded=true" width="640" height="1889" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
</div>
