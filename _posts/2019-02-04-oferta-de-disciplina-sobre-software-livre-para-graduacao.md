---
layout: post
author: Filipe Saraiva
category: pencil-square
title: Oferta de disciplina sobre software livre para graduação
---

A [Faculdade de Computação](http://computacao.ufpa.br/), subunidade a qual o __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) é vinculado,  disponibilizou para o período 2018.2 a disciplina optativa "Tópicos Especiais em Computação I (Software Livre)".

A ser ministrada pelos professores [Filipe Saraiva](../people/filipe-saraiva/) e [Gustavo Pinto](../people/gustavo-pinto), a disciplina apresentará diversos temas relacionados ao software livre em geral, desde processos de desenvolvimento, gerenciamento de times, infraestrutura, licenciamento, estudos sobre mineração de repositórios de código aberto, e mais.

Também contaremos com depoimentos por via conferência de participantes de diferentes comunidades e pesquisadores do tema, possibilitando aos nossos alunos um bom _overview_ sobre o funcionamento destes grupos e projetos, em especial sobre as maneiras como eles gerenciam seus trabalhos e demandas.

Ao final, entre outros trabalhos, a disciplina objetiva que o aluno consiga fazer alguma contribuição em algum projeto de software livre.

Segue abaixo a ementa, bibliografia e maiores detalhes sobre o curso.

__Ementa__

1. O que é software livre?
1. Processo de desenvolvimento de software livre
    1. Infraestrutura técnica
    1. Infraestrutura social e política
    1. Comunicação
    1. Gerencia de participantes
1. Licenças de Software
1. Contribuindo com projetos de software livre
1. Estudos de caso e relatos de experiência

__Referências__

* FOGEL, Karl. [Producing Open Source Software - How to Run a Successful Free Software Project](http://producingoss.com). O'Reilly, 2017.
* RAYMOND, Eric. [A Catedral e o Bazar](https://www.ufrgs.br/soft- livre-edu/arquivos/a-catedral-e-o-bazar-eric-raymond.pdf). 2010. 
* TORRES, Aracele. A Tecnoutopia do Software Livre - Uma História do Projeto Técnico e Político do GNU. Alameda, 2018. ([Tese](http://www.teses.usp.br/teses/disponiveis/8/8138/tde-31032014-111738/pt-br.php))
* PINTSCHER, Lydia (org). [Open Advice - FOSS: What we wish we had known when we started](http://open-advice.org/). 2012.
* SABINO, Vanessa; KON, Fábio. [Licenças de Software Livre - História e Características](http://ccsl.ime.usp.br/files/relatorio-licencas.pdf). CCSL-USP, 2009.

__Serviço__

* __Atividade__: Disciplina de graduação Tópicos Especiais em Computação I (Software Livre) - Código EN05248
* __Data__: Período 2018.2
* __Horário__: Sextas-feiras das 14:50 às 18:20
* __Local__: Labcomp-03 - FACOMP/ICEN
